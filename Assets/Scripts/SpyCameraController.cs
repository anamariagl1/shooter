using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpyCameraController : MonoBehaviour
{
    [Header("CAMERA")]
    [SerializeField]
    private GameObject m_Camera;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ActivateCamera()
    {
        m_Camera.SetActive(!m_Camera.activeSelf);
    }
}
