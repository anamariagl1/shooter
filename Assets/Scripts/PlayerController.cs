using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour, IDamageable
{
    [Header("PARAMETERS")]
    [SerializeField]
    private int m_Damage;
    [SerializeField]
    private int m_ShotgunDamage;
    [SerializeField]
    private int m_ShotgunProjectiles; //numero de proyectiles que dispara
    [SerializeField]
    private float m_ShotgunImprecision;
    [SerializeField]
    private float m_Speed;
    [SerializeField]
    private float m_RotationSpeed = 180f;
    [SerializeField]
    private float m_JumpHeight;
    [SerializeField]
    private float m_HpMax;
    [SerializeField]
    private float m_ShootCooldown;
    [SerializeField]
    private float m_ShotDistance;
    [SerializeField]
    private float m_SpyCamDistance;

    [Header("INPUTS")]
    [SerializeField]
    private InputActionAsset m_InputActionAsset;
    private InputActionAsset m_Input;
    private InputAction m_MovementAction;
    private InputAction m_PointerAction;

    [Header("STATE MACHINE")]
    [SerializeField]
    private SwitchMachineStates m_CurrentState;
    private enum SwitchMachineStates { NONE, IDLE, WALK, SHOOT, DEAD };

    [Header("VARIABLES")]
    [SerializeField]
    private bool m_Cooldown;
    [SerializeField]
    private float m_Hp;
    [SerializeField]
    private LayerMask m_ShootMask;
    [SerializeField]
    private LayerMask m_ObstacleMask;
    [SerializeField]
    private int m_ObstacleLayer;

    [Header("CAMERA")]
    [SerializeField]
    private GameObject m_Camera;
    [SerializeField]
    private GameObject m_SpyCamera;
    [SerializeField]
    private float m_Sensitivity;
    [SerializeField]
    private float m_Rotate = 0;

    [Header("GAME EVENTS")]
    [SerializeField]
    private GameEvent m_ChangeCamera;
    [SerializeField]
    private GameEvent m_GameOverEvent;

    private Rigidbody m_Rigidbody;
    void Awake()
    {
        m_Input = Instantiate(m_InputActionAsset);
        m_MovementAction = m_Input.FindActionMap("Default").FindAction("Movement");
        m_Input.FindActionMap("Default").FindAction("Shoot").performed += Shoot;
        m_Input.FindActionMap("Default").FindAction("ShootShotgun").performed += ShootShotgun;
        m_Input.FindActionMap("Default").FindAction("ThrowSpy").performed += ThrowSpy;
        m_Input.FindActionMap("Default").FindAction("SwitchCamera").performed += SwitchCamera;
        m_PointerAction = m_Input.FindActionMap("Default").FindAction("Pointer");
        m_Input.FindActionMap("Default").Enable();

        m_Camera = transform.Find("Main Camera").gameObject;
        m_Rigidbody = gameObject.GetComponent<Rigidbody>();
        m_ObstacleLayer = LayerMask.NameToLayer("Obstacles");
    }

    void Start()
    {
        m_Cooldown = false;
        m_Hp = m_HpMax;
        InitState(SwitchMachineStates.IDLE);
    }
    void Update()
    {
        UpdateState();

        if (m_PointerAction.ReadValue<Vector2>().x != 0)
        {
            m_Rotate += m_PointerAction.ReadValue<Vector2>().x * m_Sensitivity * Time.deltaTime;
            gameObject.transform.localEulerAngles = Vector3.up * m_Rotate;
        }

    }

    private void OnDestroy()
    {
        m_Input.FindActionMap("Default").FindAction("Shoot").performed -= Shoot;
        m_Input.FindActionMap("Default").Disable();
    }

    private void Shoot(InputAction.CallbackContext context)
    {
        //Debug.Log("Shoot");
        RaycastHit hit;
        if (Physics.Raycast(m_Camera.transform.position, m_Camera.transform.forward, out hit, m_ShotDistance, m_ShootMask))
        {
            //Debug.Log($"He tocat {hit.collider.gameObject} a la posicio {hit.point} amb normal {hit.normal}");
            Debug.DrawLine(m_Camera.transform.position, hit.point, Color.green, 2f);

            GameObject root = hit.collider.gameObject.transform.root.gameObject;
            if (root.TryGetComponent(out IDamageable target))
                target.Damage(m_Damage, hit.collider.name);
        }
    }

    private void ShootShotgun(InputAction.CallbackContext context)
    {
        RaycastHit hit;
        for (int i = 0; i < m_ShotgunProjectiles; i++)
        {
            float randomPosition = Random.Range(-m_ShotgunImprecision, m_ShotgunImprecision);
            Vector3 direction = m_Camera.transform.forward + m_Camera.transform.right* randomPosition + m_Camera.transform.up * randomPosition;

            direction.Normalize();
            if (Physics.Raycast(m_Camera.transform.position, direction, out hit, m_ShotDistance, m_ShootMask))
            {
                //Debug.Log($"He tocat {hit.collider.gameObject} a la posicio {hit.point} amb normal {hit.normal}");
                Debug.DrawLine(m_Camera.transform.position, hit.point, new Color(direction.x, direction.y, direction.z), 2f);

                GameObject root = hit.collider.gameObject.transform.root.gameObject;
                if (root.TryGetComponent(out IDamageable target))
                    target.Damage(m_ShotgunDamage, hit.collider.name);
            }
        }
    }

    private void ThrowSpy(InputAction.CallbackContext context)
    {
        //Debug.Log("throw spy");
        RaycastHit hit;
        if (Physics.Raycast(m_Camera.transform.position, m_Camera.transform.forward, out hit, m_SpyCamDistance, m_ObstacleMask))
        {
            Debug.Log($"Hit collider: " + hit.collider.gameObject.layer);
            Debug.DrawLine(m_Camera.transform.position, hit.point, Color.green, 2f);
            if (hit.collider.gameObject.layer == m_ObstacleLayer)
            {
                m_SpyCamera.SetActive(true);
                m_SpyCamera.transform.position = hit.point;
                m_SpyCamera.transform.forward = hit.normal;
            }
        }
    }

    private IEnumerator Cooldown()
    {
        m_Cooldown = true;
        yield return new WaitForSeconds(m_ShootCooldown);
        m_Cooldown = false;
    }

    public void Damage(int amount, string bodyPart)
    {
        Debug.Log("Player: Me dieron, con " + amount + " de da�o");
        m_Hp -= amount;
        if (m_Hp <= 0)
            ChangeState(SwitchMachineStates.DEAD);
    }

    public void SwitchCamera(InputAction.CallbackContext context)
    {
        m_Camera.SetActive(!m_Camera.activeSelf);
        m_ChangeCamera.Raise();
    }

    //****************************************************** STATE MACHINE ***************************************************
    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == m_CurrentState)
            return;
        if (m_CurrentState == SwitchMachineStates.DEAD) return;
        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                m_Rigidbody.velocity = Vector3.zero;
                break;

            case SwitchMachineStates.WALK:
                break;

            case SwitchMachineStates.SHOOT:
                if (m_Cooldown)
                {
                    ChangeState(SwitchMachineStates.IDLE);
                    return;
                }

                StartCoroutine(Cooldown());

                break;

            case SwitchMachineStates.DEAD:
                gameObject.SetActive(false);
                m_GameOverEvent.Raise();
                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                if (m_MovementAction.ReadValue<Vector3>().x != 0 || m_MovementAction.ReadValue<Vector3>().y != 0 || m_MovementAction.ReadValue<Vector3>().z != 0)
                    ChangeState(SwitchMachineStates.WALK);

                break;
            case SwitchMachineStates.WALK:
                Vector3 movement = Vector3.zero;
                if (m_MovementAction.ReadValue<Vector3>().x == 0)
                {
                    if (m_MovementAction.ReadValue<Vector3>().z < 0)
                        movement -= transform.forward;
                    if (m_MovementAction.ReadValue<Vector3>().z > 0)
                        movement += transform.forward;

                    m_Rigidbody.velocity = movement.normalized * m_Speed + Vector3.up * m_Rigidbody.velocity.y;
                }
                if (m_MovementAction.ReadValue<Vector3>().x > 0)
                {
                    transform.Rotate(Vector3.up * m_RotationSpeed * Time.deltaTime);
                }
                else if (m_MovementAction.ReadValue<Vector3>().x < 0)
                {

                    transform.Rotate(-Vector3.up * m_RotationSpeed * Time.deltaTime);
                }

                if (m_MovementAction.ReadValue<Vector3>() == Vector3.zero)
                    ChangeState(SwitchMachineStates.IDLE);

                break;

            case SwitchMachineStates.SHOOT:

                break;

            default:

                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                break;

            case SwitchMachineStates.WALK:
                break;

            case SwitchMachineStates.SHOOT:
                break;

            default:
                break;
        }
    }
}
