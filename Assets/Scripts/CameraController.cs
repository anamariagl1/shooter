using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CameraController : MonoBehaviour
{
    [Header("INPUTS")]
    [SerializeField]
    private InputActionAsset m_InputActionAsset;
    private InputActionAsset m_Input;
    private InputAction m_PointerAction;

    [Header("PARAMETERS")]
    [SerializeField]
    private float m_Sensitivity;
    [SerializeField]
    private float m_MinRotation;
    [SerializeField]
    private float m_MaxRotation;
    [SerializeField]
    private float m_Rotate = 0;

    void Awake()
    {
        m_Input = Instantiate(m_InputActionAsset);
        m_PointerAction = m_Input.FindActionMap("Default").FindAction("Pointer");
        m_Input.FindActionMap("Default").Enable();
    }
    void Start()
    {
       // Cursor.visible = false;
       Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        if (m_PointerAction.ReadValue<Vector2>().y != 0)
        {
            //No se mira localEulerAngles para conseguir el yRotate, solo se asigna al final
            m_Rotate -= m_PointerAction.ReadValue<Vector2>().y * m_Sensitivity * Time.deltaTime;
            m_Rotate = Mathf.Clamp(m_Rotate, m_MinRotation, m_MaxRotation);
            gameObject.transform.localEulerAngles = Vector3.right * m_Rotate;
        }
    }
}
