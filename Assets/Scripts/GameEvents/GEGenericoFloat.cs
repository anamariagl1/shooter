using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/Float")]
public class GEGenericoFloat : GEGenerico<float> { }
