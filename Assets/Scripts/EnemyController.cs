using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour, IDamageable
{

    [Header("NAVMESH")]
    [SerializeField]
    private Transform m_Target;
    [SerializeField]
    private NavMeshAgent m_Agent;
    [SerializeField]
    private float m_LimitMapMaxX;
    [SerializeField]
    private float m_LimitMapMinX;
    [SerializeField]
    private float m_LimitMapMaxY;
    [SerializeField]
    private float m_LimitMapMinY;
    [SerializeField]
    private float m_LimitMapMaxZ;
    [SerializeField]
    private float m_LimitMapMinZ;
    [SerializeField]
    private float m_MaxDistance;
    [SerializeField]
    private Vector3 m_Destination;


    [Header("STATE MACHINE")]
    [SerializeField]
    private SwitchMachineStates m_CurrentState;
    private enum SwitchMachineStates { NONE, IDLE, WALK, CHASE, SHOOT, DEAD };

    [Header("PARAMETERS")]
    [SerializeField]
    private int m_Damage;
    [SerializeField]
    private float m_HpMax;
    [SerializeField]
    private float m_ShootCooldown;
    [SerializeField]
    private float m_ShootRange;
    [SerializeField]
    private float m_ShotImprecision;
    [SerializeField]
    private float m_Speed;
    [SerializeField]
    private float m_WaitTime; //tiempo de espera despues de perder de vista al player

    [Header("VARIABLES")]
    [SerializeField]
    private bool m_Cooldown;
    [SerializeField]
    private float m_Hp;

    [Header("DAMAGE MODIFIERS")]
    [SerializeField]
    private float m_DmgModifierChest;
    [SerializeField]
    private float m_DmgModifierArm;
    [SerializeField]
    private float m_DmgModifierLeg;
    [SerializeField]
    private float m_DmgModifierHead;

    [Header("DETECTION")]
    [SerializeField]
    private int m_PlayerLayer;
    [SerializeField]
    private LayerMask m_PlayerLayerMask;
    [SerializeField]
    private LayerMask m_ObstaclesLayerMask;
    [SerializeField]
    private float m_DetectionRadius;
    [SerializeField]
    private float m_MaxDetectionDistance;

    [Header("ANIMATIONS")]
    [SerializeField]
    private Animator m_Animator;

    private void Awake()
    {
        m_Agent = GetComponent<NavMeshAgent>();
        m_PlayerLayer = LayerMask.NameToLayer("Player");
    }

    private void Start()
    {
        InitState(SwitchMachineStates.WALK);
        m_Hp = m_HpMax;
    }

    private void Update()
    {
        UpdateState();
    }

    private void DetectarPlayer()
    {
        RaycastHit hit;

        Collider[] colliders = Physics.OverlapSphere(transform.position, m_DetectionRadius, m_PlayerLayerMask);

        if (colliders.Length > 0)
        {
            //Debug.DrawLine(transform.position, colliders[0].transform.position, Color.cyan, 2f);
            Vector3 direction = (colliders[0].transform.position - transform.position).normalized;
            Ray ray = new Ray(transform.position, direction);

            if (Physics.Raycast(ray, out hit, m_MaxDetectionDistance, m_ObstaclesLayerMask))
            {
                //Debug.DrawLine(transform.position, hit.point, Color.cyan, 2f);
                //Debug.Log("Veo algo, collider: " + hit.collider.gameObject.name);
                if (hit.collider.gameObject.layer == m_PlayerLayer)
                {
                    //Debug.Log($"Veo al player con posicion {hit.point}");
                    m_Target = hit.transform;
                }
            }

        }
        else
        {
            m_Target = null;
        }

    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, m_DetectionRadius);
    }

    private void Shoot()
    {
        //Debug.Log("Enemy shoots");
        RaycastHit hit;

        float randomPosition = Random.Range(-m_ShotImprecision, m_ShotImprecision);
        Vector3 origin = new Vector3(transform.position.x, transform.position.y + 1.3f, transform.position.z);
        Vector3 direction = m_Target.position - origin;
        Vector3 shot = new Vector3(direction.x + randomPosition, direction.y + randomPosition, direction.z);

        transform.forward = direction;

        if (Physics.Raycast(origin, shot, out hit, 20f, m_ObstaclesLayerMask))
        {
            Debug.Log("Tocado :" + hit.point);
            Debug.DrawLine(origin, hit.point, Color.red, 2f);

            if (hit.collider.gameObject.transform.parent.TryGetComponent(out IDamageable target))
            {
                target.Damage(m_Damage, hit.collider.name);
            }
        }
        StartCoroutine(Cooldown());
    }

    private void Patrullar()
    {
        //Debug.Log("Posicion: " + transform.position + " Destino: " + m_Destination);

        //Comprobar si ha llegado a la posicion o aun no tenia un destino (0, 0, 0)
        if ((Mathf.Abs(transform.position.x - m_Destination.x) < 10 && Mathf.Abs(transform.position.z - m_Destination.z) < 10) || (m_Destination.x == 0 && m_Destination.y == 0 && m_Destination.z == 0))
        {
            Vector3 randomPoint = new Vector3(Random.Range(m_LimitMapMinX, m_LimitMapMaxX), Random.Range(m_LimitMapMaxY, m_LimitMapMinY), Random.Range(m_LimitMapMinZ, m_LimitMapMaxZ));
            NavMeshHit hit;
            if (NavMesh.SamplePosition(randomPoint, out hit, m_MaxDistance, NavMesh.AllAreas))
            {
                m_Destination = randomPoint;
            }
        }
        m_Agent.SetDestination(m_Destination);
    }

    public void Damage(int amount, string bodyPart)
    {
        //Debug.Log("Me han dado en " + bodyPart);
        float dmgModifier = 0;

        if (bodyPart.ToLower().Contains("hips") || bodyPart.ToLower().Contains("spine"))
            dmgModifier = m_DmgModifierChest;
        else if (bodyPart.ToLower().Contains("leg"))
            dmgModifier = m_DmgModifierLeg;
        else if (bodyPart.ToLower().Contains("arm"))
            dmgModifier = m_DmgModifierArm;
        else if (bodyPart.ToLower().Contains("head"))
            dmgModifier = m_DmgModifierHead;

        m_Hp -= amount * dmgModifier;
        //Debug.Log("Me han hecho " + amount * dmgModifier + " de da�o");

        if (m_Hp <= 0)
            ChangeState(SwitchMachineStates.DEAD);
    }

    private IEnumerator Cooldown()
    {
        m_Cooldown = true;
        yield return new WaitForSeconds(m_ShootCooldown);
        m_Cooldown = false;
    }

    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(m_WaitTime);
        ChangeState(SwitchMachineStates.WALK);
    }

    //****************************************************** STATE MACHINE ***************************************************
    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == m_CurrentState)
            return;

        //Debug.Log("CAMBIANDO A "+newState);
        if (m_CurrentState == SwitchMachineStates.DEAD)
            return;
        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                m_Agent.speed = 0;
                StartCoroutine(Wait());
                m_Animator.Play("Idle");
                break;

            case SwitchMachineStates.WALK:
                m_Agent.speed = m_Speed;
                m_Animator.Play("Walking");
                break;

            case SwitchMachineStates.CHASE:
                m_Agent.speed = m_Speed;
                m_Animator.Play("Walking");
                break;

            case SwitchMachineStates.SHOOT:
                m_Agent.speed = 0;
                m_Animator.Play("Shooting");
                break;

            case SwitchMachineStates.DEAD:
                m_Animator.Play("Dying");
                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                DetectarPlayer();
                if (m_Target != null)
                    ChangeState(SwitchMachineStates.CHASE);
                break;

            case SwitchMachineStates.WALK:
                Patrullar();
                DetectarPlayer();
                if (m_Target != null)
                    ChangeState(SwitchMachineStates.CHASE);
                break;

            case SwitchMachineStates.CHASE:
                DetectarPlayer();
                if (m_Target == null)
                {
                    ChangeState(SwitchMachineStates.IDLE);
                    return;
                }
                else if ((m_Target.position - transform.position).magnitude <= m_ShootRange)
                    ChangeState(SwitchMachineStates.SHOOT);
                else if (!m_Agent.pathPending)
                {
                    m_Agent.SetDestination(m_Target.position);
                }
                //Debug.Log("Distancia: " + (m_Target.position - transform.position).magnitude);
                break;

            case SwitchMachineStates.SHOOT:
                DetectarPlayer();
                if (!m_Cooldown)
                    Shoot();
                if (m_Target != null && (m_Target.position - transform.position).magnitude > m_ShootRange)
                    ChangeState(SwitchMachineStates.CHASE);
                break;

            default:

                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                StopAllCoroutines();
                break;

            case SwitchMachineStates.WALK:
                break;

            case SwitchMachineStates.CHASE:
                m_Target = null;
                break;

            case SwitchMachineStates.SHOOT:
                break;

            default:
                break;
        }
    }
}

